#include "clock.h"


GPIO_TypeDef * digitPortArr[NUMDIGITS] = 	{ GPIOB, GPIOB, GPIOB, GPIOB};
uint16_t digitPinArr[NUMDIGITS] = 			{ GPIO_PIN_5, GPIO_PIN_6, GPIO_PIN_7, GPIO_PIN_8 };

void initClock()
{
	for (int i = 0; i < 4; i++)
		timeSeg[i] = 0;
	currentSeg = 0;
};

uint8_t getTime()
{
	return 0;
};

uint8_t convertTime(int time)
{
	return 0;
};


void setActiveSeg(uint8_t seg)
{
	//set active segment, and turn off all other segments
	for (int i = 0; i < 4; i++)
	{
		if (seg == i)
		{
			//turn on segment
			HAL_GPIO_WritePin(digitPortArr[i], digitPinArr[i], GPIO_PIN_SET);
		}
		else
		{	
			//turn off segment
			HAL_GPIO_WritePin(digitPortArr[i], digitPinArr[i], GPIO_PIN_RESET);
		}
	}
};

//to be used in interrupt - currentSeg internally monitors which segment we're looking at 
void displayTime()
{
	//clear segment before setting current
	int i = 0;
	//setActiveSeg(currentSeg);
	clearSegLED(currentSeg);
	setSegNum(timeSeg[i]);
	dispSegNum(1);
	currentSeg = (currentSeg + 1) % 4;
};
