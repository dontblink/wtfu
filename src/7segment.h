#ifndef _7SEGMENT
#define _7SEGMENT

#include "stm32f1xx_hal.h"
#include "global.h"

/*

  Use of this file: only controls the anode pins of the 7 segment display - if they are multiplexed, cathode needs to be handled seperately

*/ 

/* PIN DEFINES */
#define NUMSEGPINS	8
#define segGPin 	0
#define segFPin 	1
#define segAPin 	2
#define segBPin 	3
#define segEPin 	4
#define segDPin 	5
#define segCPin 	6
#define segDPPin 	7

/* 7 SEGMENT BITMASK */

#define segG 	0x01
#define segF 	0x02
#define segA 	0x04
#define segB 	0x08
#define segE 	0x10
#define segD 	0x20
#define segC 	0x40
#define segDP 	0x80

#define SEGMENTMAX 8
#define CLEARSEG 0xFF

//initialize pins for output
void initSeg();

//returns 0 if invalid; otherwise, outputs the bitmask needed to display the number
uint8_t setSegNum(uint8_t num);
uint8_t setSegLED(uint8_t idx);
uint8_t clearSegLED(uint8_t idx);

void dispSegNum(uint8_t segMask);
void dispSegNumWithColon(uint8_t segMask);


//clears the 7 segment display
void clearSegNum();
#endif /*_7SEGMENT*/
