

#include "7segment.h"

GPIO_TypeDef * segPortArr[SEGMENTMAX] = { 	GPIOB, 			//Segment A
											GPIOB, 			//Segment B
											GPIOB, 			//Segment C
											GPIOB, 			//Segment D
											GPIOB, 			//Segment E
											GPIOB, 			//Segment F
											GPIOA, 			//Segment G
											GPIOA 			//Segment DP
										};

uint16_t segPinArr[SEGMENTMAX] = 	{
											GPIO_PIN_10,	//Segment A
											GPIO_PIN_11,	//Segment B
											GPIO_PIN_12,	//Segment C
											GPIO_PIN_13,	//Segment D
											GPIO_PIN_14,	//Segment E
											GPIO_PIN_15,	//Segment F
											GPIO_PIN_8,		//Segment G
											GPIO_PIN_9		//Segment DP
										};
uint8_t segMaskArr[] = {segG, segF, segA, segB, segE, segD, segC, segDP};

void initSeg()
{
	//lol
	//init pins as outputs here
	//do other shit idk
};


/*
		 A
		 _
	F	|_|	B 
	E	|_| C
		 D
middle segment - G */

uint8_t setSegNum(uint8_t num)
{
	uint8_t segMask = 0;
	switch(num)
	{
		case 0:
			segMask = segA | segB | segC | segD | segE | segF;
			break;

		case 1:
			segMask = segB | segC; 
			break;

		case 2:
			segMask = segA | segB | segG | segE | segD;
			break;

		case 3:
			segMask = segA | segB | segG | segC | segD;
			break;

		case 4:
			segMask = segF | segG | segB | segC;
			break;

		case 5:
			segMask = segA | segF | segG | segC | segD;
			break;

		case 6:
			segMask = segA | segF | segE | segG | segC | segD;
			break;

		case 7:
			segMask = segA | segB | segC;
			break;

		case 8:
			segMask = segA | segB | segC | segD | segE | segF | segG;
			break;

		case 9: segMask = segA | segB | segF | segG | segC;
			break;


		case CLEARSEG:
		default: 
			segMask = 0;
			break;
	}
	return segMask;
};

//based off array index
uint8_t setSegLED(uint8_t idx)
{
	if (idx < SEGMENTMAX)
		HAL_GPIO_WritePin(segPortArr[idx], segPinArr[idx], GPIO_PIN_SET);
	return 0;
};

uint8_t clearSegLED(uint8_t idx)
{
	if (idx < SEGMENTMAX)
		HAL_GPIO_WritePin(segPortArr[idx], segPinArr[idx], GPIO_PIN_RESET);
	return 0;
};

void dispSegNum(uint8_t segMask)
{
	//turn relevant pins on
	for (int i = 0; i < SEGMENTMAX; i++)
	{
		if (segMask & segMaskArr[i])
			setSegLED(i);
		else
			clearSegLED(i);
	}
};

void dispSegNumWithColon(uint8_t segMask)
{
	//add the colon to the mask, then output
	segMask |= segDP;
	dispSegNum(segMask);

};

void clearSegNum()
{
	dispSegNum(CLEARSEG);
}
