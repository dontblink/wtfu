#ifndef _CLOCK_H
#define _CLOCK_H

#include "7segment.h"

/* 

these functions simulate clock functionality - will need to use 7 segment LED control multiplexed over 4 displays
Aim for 30Hz or better switching

*/

/* PIN DEFINES */
#define NUMDIGITS
#define HR_1PIN		1
#define HR_2PIN		2
#define MIN_1PIN	3
#define MIN_2PIN	4

#define HR1	 	1
#define HR2 	2
#define MIN1 	3
#define MIN2 	4

uint8_t timeSeg[4];
uint8_t currentSeg;

void initClock();
uint8_t getTime();
uint8_t convertTime(int time); 
void setActiveSeg(uint8_t seg);
void displayTime();

#endif /* _CLOCK_H */
